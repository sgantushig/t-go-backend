const { Router } = require('express');

const VoiceTable = require('../voice/table');

const router = new Router();

router.get('/', (req, res) => {

    VoiceTable.getVoice()
    .then((voice) => {
        res.json( voice );
    })
    .catch(error => res.send(error));
});

module.exports = router;