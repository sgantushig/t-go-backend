const { Router } = require('express');
const router = new Router();

const OrdersTemporaryTable = require('../ordersTemporary/table');


router.get('/', (req, res) => {
    OrdersTemporaryTable.getOrdersTemporary()
      .then(({ordersTemporary}) => {
        res.json(ordersTemporary)
      })
      .catch(error => console.log(error))
});

router.post('/', (req, res) => {

  const { driverId, coordinatesCurrent, estimatedTime, minkm, status} = req.body;

  OrdersTemporaryTable.storeOrdersTemporary(
    driverId,
    coordinatesCurrent, 
    estimatedTime, 
    minkm,
    status
  )
});


module.exports = router;