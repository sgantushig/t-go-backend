const { Router } = require('express');

const { authenticatedAccount } = require('./helper');

const VoiceTable = require('../voice/table');
const DriveTable = require('../drive/table');
const OrdersTemporaryTable = require('../ordersTemporary/table');
const OrdersTable = require('../orders/table');

const router = new Router();


router.get('/', (req, res, next) => {
      
    DriveTable.getDrive()
        .then(({drive}) => {
            drive.sort((a, b) => parseFloat(b.id) - parseFloat(a.id));
            res.json(drive);
        })
        .catch(error => next(error));
});

router.post('/', (req, res, next) => {

    var { token, coordinates } = req.body;
    
    authenticatedAccount(token)
        .then(({ account }) => {
            var voice = null;
            
            VoiceTable.getVoiceById(account.id)
                .then(({voice}) => {
                    //var account = account;
                    account.voice = voice;
                    console.log("A1", voice,"driver :", account.driver, coordinates,"voice :", account.voice);

                    //VoiceTable.deleteVoiceById(account.id);

                    if (account.driver) {
                        DriveTable.storeDrive(account.id, coordinates);
                        console.log("A2", "success", account.voice);
                            OrdersTemporaryTable.getOrderTemporaryCoordinatesClient(account.id)
                                .then(({coordinatesClient}) => {
                                    //console.log("A1", coordinatesClient);
                                    if (coordinatesClient) {
                                        // JSON.parse(coordinatesClient)
                                        console.log("D3", voice);
                                        
                                        res.json(
                                            {
                                                "order": {
                                                "customer": {
                                                    "lat": 124,
                                                    "lng": 567
                                                },
                                                "address": "Улаанбаатар их дэлгүүр",
                                                account
                                            }
                                            // "driver": {
                                            //     "name": "Bat",
                                            //     "cardNo": "123",
                                            //     "avatar": "http://127.0.0.1/driver/avatar.jpg",
                                            //     "coordinate": {
                                            //         "lat": 124,
                                            //         "lng": 567
                                            //     }
                                            // },
                                            // // tracking - DRIVING төлөвт орсон үед явсан
                                            // "tracking": [
                                            //     {
                                            //         "lat": 124,
                                            //         "lng": 567
                                            //     },
                                            //     {
                                            //         "lat": 123,
                                            //         "lng": 567
                                            //     },
                                            //     {
                                            //         "lat": 123,
                                            //         "lng": 567
                                            //     }
                                            // ],
                                            // "createdAt": 324354545,
                                            // "expiredAt": 324354545,
                                            // "status": "INIT" //DRVING, STOPPED, BILLING, ...
                                            
                                            }
                                        );
                                    } else {
                                        res.json({"order": null});
                                    }
                                })
                                .catch(error => res.json({"order2": null}) );

                    } else {
                        OrdersTable.getOrder(account.id)
                                .then(({order}) => {
                                    console.log("C3", order.driverId, voice);
                                    res.json({"order.driverId" : "voice",
                                    account});
                                })
                                .catch(error => console.log(error) );
                    }   
                })
                .catch(error => next(error));
          
        })
        .catch(error => next(error));
});

module.exports = router; 