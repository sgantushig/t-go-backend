const AccountTable = require('../account/table');
const { APP_SECRET } = require('../../secrets');
const jwt = require('jsonwebtoken');

const setSession = ({id, tel, pinHash, res, driver1 }) => {
    return new Promise((resolve, reject) => {
      setSessionToken({id, tel, pinHash, res, driver1 });
    });
  }

const setSessionToken = ({id, tel, pinHash, res, driver1}) => {
 
    const token = jwt.sign({id, tel, pinHash }, APP_SECRET, { expiresIn: '1h'});

    res.json({"token": token,
              "otp": true,
              "driver": driver1,
              "msg": "success" });
};

const deleteSessionToken = ({res}) => {
    res.json({"token": null,
              "msg": "token deleted" });
};

const authenticatedAccount = (token) => {
  return new Promise((resolve, reject) => {
    if (!token) {
      const error = new Error('Invalid token');
  
      error.statusCode = 400;
  
      return reject(error);
    } else {
      const {id, tel, pinHash} = jwt.decode(token, APP_SECRET);
      let authenticated = false;
     AccountTable.getAccount(tel)
         .then(({ account }) => {
          if( account && account.pinHash == pinHash) {
           authenticated = true;
          } 
          resolve({ account, authenticated});
         })
         .catch(error => reject(error));
    }
  });
};

module.exports = { setSession, deleteSessionToken, authenticatedAccount };