const { Router } = require('express');
const multer  = require('multer');
const fs = require('file-system');
var async = require('async');

const AccountTable = require('../account/table');
const { hash } = require('../account/helper');
const { setSession, deleteSessionToken, authenticatedAccount } = require('./helper');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __dirname + '/assets/tmp/')
  
  },
  filename: (req, file, cb) => {
    cb(null,  'avatar.png')
  }
}); 
const upload = multer({storage:storage});

const router = new Router();

router.post('/login', (req, res, next) => {
  const { tel, pin, driver } = req.body;

  AccountTable.getAccount( tel )
    .then(({ account }) => {
      if
      //  (!account && !pin) {

      //   const otp = false;
      //   res.json({otp});

      // } else if 
      (!account && pin) {

        pinHash = hash(pin);
        AccountTable.storeAccount({ tel, pinHash, driver })
          .then (() => {
            AccountTable.getAccount(tel)
              .then(({account}) => {
                driver1 = account.driver;
                id = account.id;
                return setSession({ id, tel, pinHash, res, driver1 });
              })
              .catch(error => next(error));
              //driver1 = account.driver;
          
          })
          .catch(error => next(error));

        
      // } else if (account && !pin) {
      //   const otp = true;
      //   res.json({otp});

      } else if (account && account.pinHash === hash(pin)) {

        console.log(account);

        const { id, tel, pinHash } = account;

        driver1 = account.driver;
      
        return setSession({ id, tel, pinHash, res, driver1 });
        
      } else {
        const msg = 'Incorrect tel/pin';

        res.json({msg});
      }
    })
    .catch(error => next(error));
});

router.get('/logout', (req, res, next) => {
  deleteSessionToken({res});
});

router.post('/authenticated', (req, res, next) => {

  authenticatedAccount(req.body.token )
    .then(({ authenticated }) => res.json({ authenticated }))
    .catch(error => next(error));
});

router.post('/profile', upload.fields([{name: 'file', maxCount: 1}, {name: 'info', maxCount: 1}]), (req, res, next) => {

  const { token, pin, name, action, avatar } = JSON.parse(req.body.info);

  authenticatedAccount(token)
    .then(({ account }) => {
      switch (action) {
        case 'get' :
        
          res.json({account});

          console.log(account);
        break;

        case 'set' :
      
          if (pin) account.pinHash = hash(pin);
          if (name) account.name = name;
          if (avatar) {
            account.avatar = __dirname + '/assets/'+account.tel+'.png';
            
            async.waterfall([
              function copyFile(callback) {
                  fs.copyFile(__dirname + '/assets/tmp/avatar.png', __dirname + '/assets/'+account.tel+'.png');
                  callback(null, 'copied');
              },
              function unlinkFile(msg, callback) {
                fs.unlink(__dirname + '/assets/tmp/avatar.png', (err) => {
                  if (!err) {
                    msg += ' deleted';
                    AccountTable.updateAccount({account});
                    callback(null, msg);
                  } else {
                    callback(err);
                  }
                });
              }
            ],
            function (error, msg) {
                  if (error) {
                    console.error(err);
                      //handle readFile error or processFile error here
                  } else {
                    console.log('msg2', msg);
                  }
            }
          );
            }
          res.json({ account });
        break;

      };
    })
    .catch(error => next(error));
  
});

module.exports = router;