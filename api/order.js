const { Router } = require('express');
const multer = require('multer');
const fs = require('file-system');
var async = require('async');

const { authenticatedAccount } = require('./helper');
const { hash } = require('../account/helper');

const AccountTable = require('../account/table');
const OrdersTable = require('../orders/table');
const OrdersTemporary = require('../ordersTemporary/table');
const DriveTable = require('../drive/table');
const VoiceTable = require('../voice/table');

const router = new Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __dirname + '/assets/tmp/')

    },
    filename: (req, file, cb) => {
        cb(null, 'voice.mp3')
    }
});
const upload = multer({ storage: storage });

router.post('/get', (req, res, next) => {

    var { token, coordinates, coordinatesDriver, coordinatesDest, status, voiceChat, sugestedMinutes
    } = req.body;

    authenticatedAccount(token)
        .then(({ account }) => {       
            switch (status) {
                case "INIT":
                    const timestampInit = new Date();

                    OrdersTable.storeOrderInit(account.id, coordinates, timestampInit, account.tel)

                        .catch(error => next(error));
                    
                    var minkm = [];
                    var mindriver = [];

                    DriveTable.getOneDrive()

                        .then(({ drivers }) => {

                            drivers = drivers.filter(( obj ) => {
                                return obj.status !== false;
                            });

                            // console.log("A1", drivers);

                            var len = drivers.length
                            var i;
                            
                            minkm[0] = 100000000;
                            minkm[1]= 100000000;
                            
                            
                            mindriver[0] = drivers[0];
                            mindriver[1] = mindriver[0];

                            // console.log("A2", mindriver[0], mindriver[1]);
                            
                            for (i = 0; i < len; i++) {

                                var a = JSON.parse(drivers[i].coordinates);
                                var lat2 = a.lat;
                                var lon2 = a.lng;

                                var lat1 = coordinates.lat;
                                var lon1 = coordinates.lng;

                                var radlat1 = Math.PI * lat1 / 180;
                                var radlat2 = Math.PI * lat2 / 180;

                                var theta = lon1 - lon2;

                                var radtheta = Math.PI * theta / 180;
                                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

                                if (dist > 1) {
                                    dist = 1;
                                }
                                dist = Math.acos(dist);
                                dist = dist * 180 / Math.PI;
                                dist = dist * 60 * 1.1515;

                                if(dist < minkm[0]) {
                                  
                                    if(dist < minkm[1]) {
                                        minkm[0] = minkm[1];
                                        minkm[1] = dist;
                                        mindriver[1]=drivers[i]
                                    }
                                    else{
                                        minkm[0] = dist;
                                        mindriver[0]=drivers[i]
                                    }
                                }              
                            }
                            
                            mindriver[0] = Object.assign(mindriver[0], {'minkm' : minkm[0]});
                            mindriver[1] = Object.assign(mindriver[1], {'minkm' : minkm[1]});

                            //console.log("A2", mindriver[1], mindriver[0]);
                        })
                        .then(() => {
                            
                            for (i = 0; i < 2; i++) { 
                                OrdersTemporary.storeOrdersTemporary(mindriver[i].driverId, mindriver[i].coordinates, mindriver[i].minkm, account.id, coordinates)

                                    .catch(error => console.log(error));
                                    console.log("tushgee", account.id, coordinates,  mindriver[i]);

                                    var drivers2 = mindriver[0] 
                                    var drivers3 = mindriver[1] 
                            }

                            let drivers4 = drivers2
                            let drivers5 = drivers3

                            res.json({
                                "driver_1": drivers4,
                                "driver_2": drivers5,
                                "msg":"success"});

                                console.log("Blalala", drivers4, drivers5); 
                        })

                        .catch(error => console.log(error));
                    
                    
                break;
                
                case 'STARTED':

                    console.log("A1", account.id, sugestedMinutes);
                   
                    OrdersTemporary.updateOrderTemporary( sugestedMinutes, account.id)
                        .then(() => {
                            console.log("success");
                            OrdersTemporary.getOrderTemporaryClientId(account.id)
                            .then((OrderTemporary) => {
                                console.log("A1", OrderTemporary.clientId);

                                OrdersTemporary.getOrderTemporarySpecial(account.id, OrderTemporary.clientId)
                                    .then((minutes) => {
                                        console.log("A2", minutes.estimatedTime);
                                        if(minutes.estimatedTime !== null) {

                                            console.log("Minutaa burtguuleed, Minutiin haritsuulalt hiigeed, tentsuu minkm eer shalgaruulaad, utgalt hiine");
                                            OrdersTemporary.updateOrderTemporary(sugestedMinutes, account.id)
                                                .then(() => {
                                                    var id=OrderTemporary.clientId
                                                    OrdersTemporary.matchOrderTemporary(id)
                                                    .then((drive) => {
                                                        console.log(drive);
                                                       
                                                        if(drive[0].estimatedTime>drive[1].estimatedTime){
                                                            drive[0]=drive[1];
                                                        }
                                                        if(drive[0].estimatedTime == drive[1].estimatedTime){
                                                            if(drive[0].minkm > drive[1].minkm){
                                                                drive[0]=drive[1];
                                                            }
                                                        }
                                                        var id=OrderTemporary.clientId;
                                                        var driveId=drive[0].driverId;
                                                        var coordinatesInitDriver=drive[0].coordinatesCurrent;

                                                        const timestampStarted = new Date();

                                                        OrdersTable.updateOrderStarted(driveId, coordinatesInitDriver, timestampStarted, id)
                                                        .then(() => {

                                                                var id = OrderTemporary.clientId;
                                                              
                                                                OrdersTemporary.deleteDrive(id)
                                                                .then(()=>{
                                                                    console.log("Ustgalaa");
                                                                }
                                                            )
                                                            }
                                                        
                                                            
                                                        )
                                                    })
                                                        
                                                })

                                        } else {
                                            console.log("A3 Odooo baina shtee 10 sec bolood ");
                                            setTimeout(() => {
                                                OrdersTemporary.getOrderTemporarySpecial(account.id, OrderTemporary.clientId)
                                                    .then((minutes) => {
                                                        if(minutes.estimatedTime !== undefined ) {
                                                            console.log("Shalgaruulaad, ustgalt hiine");
                                                                                                                       
                                                        } else {

                                                            return;
                                                        }
                                                    })

                                                console.log("30 secund bolov");
                                            },30000);
                                        }
                                    })
                                
                            })
                        })
                        .catch(error => console.log(error));
                    
                    break;

                case 'ARRIVE':

                    const timestampArrive = new Date();
                    console.log("waiting", timestampArrive)

                    OrdersTable.storeOrderArrive(timestampArrive, account.id)
                    .then(() => {
                        res.json("success");
                    })
                    .catch(error => next(error));

                    break;    

                case 'WAITING':

                    const timestampWaiting = new Date();
                    console.log("waiting", timestampWaiting)

                    OrdersTable.storeOrderWaiting(timestampWaiting, account.id)
                    .then(() => {
                        res.json("success");
                    })
                    .catch(error => next(error));

                    break;

                case 'DRIVING':
                    const timestampDrive = new Date();

                    OrdersTable.storeOrderDriving(timestampDrive,  account.id)
                        .then(() => {
                            res.json("success");
                        })
                        .catch(error => next(error));
                    break;

                case 'STOPPED':

                    const timestampStopped = new Date();
                    coordinatesDest = "'" + JSON.stringify(coordinatesDest) + "'";
                    console.log("A1", coordinatesDest, timestampStopped, account.id);

                    OrdersTable.storeOrderStopped(coordinatesDest, timestampStopped, account.id)
                        .then(() => {
                            res.json("success");
                        })
                        .catch(error => next(error));
                    break;
                
                case 'BILLING':

                    const timestampBilling = new Date();
                    const timestampPayed = new Date();

                    OrdersTable.storeOrderBilling( timestampBilling, timestampPayed, account.id)
                        .then(() => {
                            res.json("success");
                        })
                        .catch(error => next(error));

                    break;

                case 'CLOSED':
                    const timestampClosed = new Date();

                    OrdersTable.storeOrderClosed( timestampClosed, account.id)
                        .then(() => {
                            res.json("success");
                        })
                        .catch(error => next(error));

                    break;

            };
        })
        .then(() => { 
        })
        .catch(error => next(error));

});

router.post('/download', (req, res) => {

    var fileName = req.body.fileName

    console.log (fileName);

    var file = __dirname + '/assets/voice/' + fileName;
    res.download(file); // Set disposition and send it.

});

router.post('/upload', upload.fields([{name: 'file', maxCount: 1}, {name: 'info', maxCount: 1}]), (req, res, next) => {

    const { token, driverId, customerId } = JSON.parse( req.body.info );
    const timestamp = new Date();

    authenticatedAccount(token, driverId)
        .then(({ account }) => {

            if(account.driver){

                var customer = customerId;
                var voicePath = (__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/voice/' + 'D' + account.id + '- C' + customer + timestamp +'.mp3');

                console.log("A6", driverId, driver);
                VoiceTable.storeVoice(

                    account.id,             //driverId
                    customerId,             //customerId 
                    voicePath,              //voicePath 
                    timestamp               //timestamp

                    );

                async.waterfall([

                    function copyFile(callback) {
                        fs.copyFile(__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/voice/' + 'D' + account.id + '- C' + customer + timestamp +'.mp3');
                        callback(null, 'copied');
                    },

                    function copyFile() {
                        fs.copyFile(__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/voice/' + account.id  + '.mp3');
                    },

                    function unlinkFile(msg, callback) {
                        fs.unlink(__dirname + '/assets/tmp/voice.mp3', (err) => {
                            if (!err) {
                                msg += ' deleted';
                            } else {
                                callback(err);
                            }
                        });
                    }
                ],

                    function (error, msg) {
                        if (error) {
                            console.error(err);
                        } else {
                            console.log('msg2', msg);
                        }
                    });

            } else {
            
                var driver = driverId;
                var voicePath = (__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/voice/' + 'C' + account.id + '- D' + driver + timestamp +'.mp3');

                console.log("A6", driverId, driver);
                VoiceTable.storeVoice(

                    account.id,             //customerId
                    driverId,               //driverId
                    voicePath,              //voicePath 
                    timestamp               //timestamp

                    );

                async.waterfall([

                    function copyFile(callback) {
                        fs.copyFile(__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/voice/' + 'C' + account.id + '- D' + driver + timestamp +'.mp3');
                        callback(null, 'copied');
                    },

                    function copyFile() {
                        fs.copyFile(__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/voice/' + account.id  + '.mp3');
                    },

                    function unlinkFile(msg, callback) {
                        fs.unlink(__dirname + '/assets/tmp/voice.mp3', (err) => {
                            if (!err) {
                                msg += ' deleted';
                            } else {
                                callback(err);
                            }
                        });
                    }
                ],

                    function (error, msg) {
                        if (error) {
                            console.error(err);
                        } else {
                            console.log('msg2', msg);
                        }
                    });
            }
            
            res.json("success");
        })
    .catch(error => next(error));

});

module.exports = router;