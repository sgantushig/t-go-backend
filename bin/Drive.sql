create table Drive (
    id              serial primary key,
    "driverId"      INTEGER ,
    coordinates     varchar,
    STATUS          Boolean NOT NULL default false, 
    timestamp       TIMESTAMP DEFAULT NOW()
);

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (1, '{"lat" : 47.922546, "lng" : 106.920679}',true );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (2, '{"lat" : 47.922646, "lng" : 106.920679}',true );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (3,'{"lat" : 47.922898, "lng" : 106.923809}',true );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (4,'{"lat" : 47.925930, "lng" : 106.927370}',true );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (5, '{"lat" : 47.929750, "lng" : 106.925877}',true );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (6, '{"lat" : 47.922419, "lng" : 106.910687}',true );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (7, '{"lat" : 47.920241, "lng" : 106.908558}',true );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (8, '{"lat" : 47.919151, "lng" : 106.918132}',false );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (9,'{"lat" : 47.916100, "lng" : 106.929375}',false );

INSERT INTO Drive ("driverId", coordinates, status)
VALUES (10,'{"lat" : 47.922546, "lng" : 106.920679}',false );
