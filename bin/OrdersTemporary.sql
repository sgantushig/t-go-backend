CREATE TABLE OrdersTemporary (
    id                      serial PRIMARY key,
    "driverId"              INTEGER,
    "coordinatesCurrent"    VARCHAR,
    "clientId"              INTEGER,
    "coordinatesClient"     VARCHAR DEFAULT NULL,    
    "estimatedTime"         VARCHAR,
    TIMESTAMP               TIMESTAMP NOT NULL DEFAULT NOW(),
    minkm                   varchar,
    STATUS                  boolean
);


INSERT INTO OrdersTemporary ("driverId", "coordinatesCurrent", TIMESTAMP, minkm, STATUS)
VALUES (2, '{"lat" : 47.909655, "lng" : 106.924958}', now(), '0.75', true);

INSERT INTO OrdersTemporary ("driverId", "coordinatesCurrent", TIMESTAMP, minkm, STATUS)
VALUES (3, '{"lat" : 47.923741, "lng" : 106.934867}', now(), '0.45', true);

INSERT INTO OrdersTemporary ("driverId", "coordinatesCurrent", TIMESTAMP, minkm, STATUS)
VALUES (4, '{"lat" : 47.912901, "lng" : 106.933448}', now(), '1.20', true);

INSERT INTO OrdersTemporary ("driverId", "coordinatesCurrent", TIMESTAMP, minkm, STATUS)
VALUES (5, '{"lat" : 47.913575, "lng" : 106.906874}', now(), '0.80', true);