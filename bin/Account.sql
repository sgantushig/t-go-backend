CREATE TABLE Account(              
    id                  SERIAL PRIMARY KEY,
    tel                 varchar(8) NOT NULL,
    "pinHash"           varchar NOT NULL,
    name                varchar,
    avatar              varchar,
    "qrCode"            varchar,
    driver              Boolean DEFAULT false,
    timestamp           timestamp DEFAULT CURRENT_TIMESTAMP,
    "startCoordinates"  varchar,    
    "endCoordinates"    varchar
);

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '99118533', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '000000000', true, now(), '{"lat" : 47.924872, "lng" : 106.911664}');

INSERT INTO Account ( tel, "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '99247524', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2' , '009999956', true, now(), '{"lat" : 47.922546, "lng" : 106.920679}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '88619121', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '789789789', true, now(),'{"lat" : 47.922898, "lng" : 106.923809}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '11111111', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '456456456', true, now(),'{"lat" : 47.925930, "lng" : 106.927370}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '88888888', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '7787778787', true, now(), '{"lat" : 47.929750, "lng" : 106.925877}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '55555555', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '8989898989', true, now(), '{"lat" : 47.922419, "lng" : 106.910687}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '22222222', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '0300303030030', true, now(), '{"lat" : 47.920241, "lng" : 106.908558}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '33333333', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '79797979797979', true, now(),'{"lat" : 47.919322, "lng" : 106.904255}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '44444444', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '7787778787', true, now(), '{"lat" : 47.917773, "lng" : 106.902575}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '91919191', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '02200020202', true, now(), '{"lat" : 47.912681, "lng" : 106.907302}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '88118533', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '000000000', true, now(), '{"lat" : 47.913327, "lng" : 106.905020}');

INSERT INTO Account ( tel, "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '88247524', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2' , '009999956', true, now(), '{"lat" : 47.903783, "lng" : 106.911448}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '99619121', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '789789789', true, now(), '{"lat" : 47.897210, "lng" : 106.908552}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '12121212', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '456456456', true, now(), '{"lat" : 47.919151, "lng" : 106.918132}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '87878787', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '7787778787', true, now(), '{"lat" : 47.916100, "lng" : 106.929375}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '51515151', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '8989898989', true, now(), null );

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '66666666', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '0300303030030', true, now(), null );




INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '79797979', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '79797979797979', false, now(), '{"lat" : 47.9188587, "lng":106.9180333}' );

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '00000000', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '7787778787', false, now(), '{"lat" : 47.917449, "lng" : 106.911631}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '77777777', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '7787778787', false, now(), '{"lat" : 47.901754, "lng" : 106.915354}');

INSERT INTO Account ( tel , "pinHash", "qrCode", driver, timestamp, "startCoordinates" )
VALUES ( '99999999', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', '02200020202', false, now(), null);