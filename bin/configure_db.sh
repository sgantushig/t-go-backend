#!/bin/bash


echo "Configuring ttap"

dropdb -U node_user ttapdb;
createdb -U node_user ttapdb;

psql -U node_user ttapdb < ./bin/sql/Account.sql
psql -U node_user ttapdb < ./bin/sql/Balance.sql
psql -U node_user ttapdb < ./bin/sql/Contacts.sql
psql -U node_user ttapdb < ./bin/sql/Counter.sql
psql -U node_user ttapdb < ./bin/sql/Customer.sql
psql -U node_user ttapdb < ./bin/sql/Devices.sql
psql -U node_user ttapdb < ./bin/sql/Driver.sql
psql -U node_user ttapdb < ./bin/sql/Driver_rejects.sql
psql -U node_user ttapdb < ./bin/sql/Faqs.sql
psql -U node_user ttapdb < ./bin/sql/Feedback.sql
psql -U node_user ttapdb < ./bin/sql/Files.sql
psql -U node_user ttapdb < ./bin/sql/Google_map.sql
psql -U node_user ttapdb < ./bin/sql/Guest.sql
psql -U node_user ttapdb < ./bin/sql/Locations.sql
psql -U node_user ttapdb < ./bin/sql/Log.sql
psql -U node_user ttapdb < ./bin/sql/Mylocations.sql
psql -U node_user ttapdb < ./bin/sql/Mylocations_default.sql
psql -U node_user ttapdb < ./bin/sql/News.sql
psql -U node_user ttapdb < ./bin/sql/Order_log.sql
psql -U node_user ttapdb < ./bin/sql/Orders.sql
psql -U node_user ttapdb < ./bin/sql/Otp.sql
psql -U node_user ttapdb < ./bin/sql/Pages.sql
psql -U node_user ttapdb < ./bin/sql/Parameter.sql
psql -U node_user ttapdb < ./bin/sql/Products.sql
psql -U node_user ttapdb < ./bin/sql/Products_category.sql
psql -U node_user ttapdb < ./bin/sql/Settings.sql
psql -U node_user ttapdb < ./bin/sql/Slides.sql
psql -U node_user ttapdb < ./bin/sql/Socket_log.sql
psql -U node_user ttapdb < ./bin/sql/Stats_sell_month.sql
psql -U node_user ttapdb < ./bin/sql/Surprise.sql
psql -U node_user ttapdb < ./bin/sql/Tracking.sql
psql -U node_user ttapdb < ./bin/sql/Users.sql
psql -U node_user ttapdb < ./bin/sql/Voice.sql
psql -U node_user ttapdb < ./bin/sql/Drive.sql
psql -U node_user ttapdb < ./bin/sql/OrdersTemporary.sql
psql -U node_user ttapdb < ./bin/sql/Chart.sql

echo "ttap configured"