create table Orders(
    id                              serial primary key,
    tel                             varchar(8) ,
    "customerId"                     integer ,
    "driverId"                       integer , 
    "coordinatesInit"                varchar,
    "coordinatesInitDriver"          varchar,
    "coordinatesDriver"              varchar,
    "coordinatesDest"                varchar,
    "coordinatesCurrent"             varchar,
    "timestampInit"                  timestamp ,
    "timestampArrive"                timestamp ,
    "timestampWaiting"               timestamp ,
    "timestampClosed"                timestamp ,
    "timestampStarted"               timestamp ,
    "timestampDrive"                 timestamp ,
    "timestampCurrent"               timestamp ,
    "timestampStopped"               timestamp ,
    "timestampBilling"               timestamp ,
    "timestampPayed"                 timestamp ,
    status                          varchar,
    extra                           varchar,
    "lastVoiceDriver"               varchar,
    description                     text ,
    comment                         text ,
    "lastVoiceDriverTimestamp"     timestamp ,
    "lastVoiceCustomer"             varchar,
    "lastVoiceCustomerTimestamp"   timestamp 
);

INSERT INTO Orders(tel, "customerId", "driverId", "coordinatesInit", "coordinatesInitDriver", "coordinatesDriver", "coordinatesDest", "coordinatesCurrent", "timestampInit", "timestampArrive", "timestampWaiting", "timestampClosed", "timestampStarted", "timestampDrive", "timestampCurrent", "timestampStopped", "timestampBilling", "timestampPayed", STATUS, extra, "lastVoiceDriver", description, comment, "lastVoiceDriverTimestamp", "lastVoiceCustomer", "lastVoiceCustomerTimestamp")
VALUES (79797979 ,18, 1, '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', now(), now(), now(),now(),now(),now(),now(),now(),now(),now(), true, 'good', null, 'good', 'best', now(), null, now());