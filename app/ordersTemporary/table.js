const pool = require('../../databasePool');

class OrdersTemporaryTable {

    static getOrderTemporaryCoordinatesClient(driverId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "coordinatesClient" FROM OrdersTemporary WHERE "driverId" = $1 ORDER BY id DESC
                LIMIT 1`,
                [driverId],
                
                (error, response) => {

                if (error) return reject(error);

                resolve(response.rows[0]);
                }
            );
        });
    };

//     select *
// from customer c
// join ( select customer_id
//        from customer
//        group by customer_id
//        having count(program_name) = 1
//      ) t on t.customer_id = c.customer_id
// where ... -- any further winnowing of the result set occurs here

    static getOrderTemporaryClientId(driverId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "clientId" FROM OrdersTemporary WHERE "driverId" = $1 ORDER BY id DESC
                LIMIT 1`,
                [driverId],
                
                (error, response) => {
                if (error) return reject(error);
                resolve( response.rows[0]);
                }
            );
        });
    };

    static getOrderTemporarySpecial(driverId, clientId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "estimatedTime" FROM OrdersTemporary WHERE "driverId" != $1 AND "clientId" = $2 ORDER BY id DESC
                LIMIT 1`,
                [driverId, clientId],
                
                (error, response) => {
                if (error) return reject(error);
                resolve( response.rows[0]);
                }
            );
        });
    };

    static getOrdersTemporary() {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM OrdersTemporary `,
                
                (error, response) => {
                if (error) return reject(error);
                resolve({ordersTemporary: response.rows});
                }
            );
        });
    };

    static storeOrdersTemporary(driverId, coordinatesCurrent, minkm, clientId, coordinatesClient, estimatedTime, status ) {
        return new Promise((resolve, reject) => {
            pool.query(
                `insert into OrdersTemporary ("driverId", "coordinatesCurrent",  minkm, "clientId", "coordinatesClient", "estimatedTime", status)
                VALUES($1, $2, $3, $4, $5, $6, $7)`,
                [driverId, coordinatesCurrent, minkm, clientId, coordinatesClient, estimatedTime, status ],
                (error, response) => {
                if (error) return reject(error);
                resolve({msg: 'success!'});
                }
            );
        });
    };

    static updateOrderTemporary( estimatedTime, driverId ) {
        return new Promise((resolve, reject) => {
            pool.query(
                `UPDATE OrdersTemporary SET "estimatedTime" = $1 WHERE "driverId" = $2`,
                [estimatedTime, driverId],
                (error, response) => {
                if (error) return reject(error);
                resolve({msg: 'success!'});
                }
            );
        });
    };
   
    // static IfOrderTemporary( clientId ) {
    //     return new Promise((resolve, reject) => {
    //         pool.query(
    //             `SELCECT driverId FROM "estimatedTime" = 'NULL' WHERE "clientId" = $2`,
    //             [estimatedTime, driverId],
    //             (error, response) => {
    //             if (error) return resolve({"false":driverId});
    //             resolve(driverId);
    //             }
    //         );
    //     });
    // };
    static matchOrderTemporary(id) {
        
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM OrdersTemporary WHERE "clientId" = $1`,
                [id],
                (error, response) => {
                if (error) return reject(error);
                resolve(response.rows);
                }
            );
        });
    };
    static deleteDrive(id) {
        
        return new Promise((resolve, reject) => {
            pool.query(
                `Delete FROM OrdersTemporary WHERE "clientId" = $1`,
                [id],
                (error, response) => {
                if (error) return reject(error);
                resolve({msg: 'success!'});
                }
            );
        });
    };
}

module.exports = OrdersTemporaryTable;

// `SELECT DISTINCT ON ("driverId") id, "driverId", coordinates, status, timestamp
// FROM Drive ORDER BY "driverId", timestamp DESC;`