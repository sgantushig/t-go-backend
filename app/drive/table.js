const pool = require('../../databasePool');

class DriveTable {

    static getDrive() {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT DISTINCT ON ("driverId") id, "driverId", coordinates, status, timestamp
                FROM Drive ORDER BY "driverId", timestamp DESC;`,

                (error, response) => {
                if (error) return reject(error);
                resolve({drive: response.rows});
                }
            );
        });
    };

    static getOneDrive() {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT DISTINCT ON ("driverId") id, "driverId", coordinates, status, timestamp
                FROM Drive ORDER BY "driverId", timestamp DESC ;`,
                
                (error, response) => {
                if (error) return reject(error);
                resolve({drivers: response.rows});
                }
            );
        });
    };

    static storeDrive( driverId, coordinates) {

        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Drive("driverId", "coordinates" )
                VALUES($1, $2)`,

                [driverId, coordinates],
                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }
}

module.exports = DriveTable;