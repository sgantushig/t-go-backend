const pool = require('../../databasePool');

class OrdersTable {
    static getOrder(id) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "customerId", "driverId", "coordinatesInit", "coordinatesInitDriver", "coordinatesDriver", "coordinatesDest", "coordinatesCurrent", 
                "timestampInit", "timestampArrive", "timestampWaiting", "timestampClosed", "timestampStarted", "timestampDrive", "timestampCurrent", "timestampStopped", "timestampBilling",
                "timestampPayed", status, extra, "lastVoiceDriver", description, comment, "lastVoiceDriverTimestamp", "lastVoiceCustomer", "lastVoiceCustomerTimestamp"
                FROM Orders
                WHERE "driverId"=$1 
                `,
                [id],
                (error, response) => {
                if (error) return reject(error);

                if (response.rows.length === 0) return reject(new Error('no Orders'));

                resolve({order : response.rows[0]});
                }
            );
        });
    }

    static getOrderCustomer(id) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT *
                FROM Orders
                WHERE "customerId"=$1 
                `,
                [id],
                (error, response) => {
                if (error) return reject(error);

                if (response.rows.length === 0) return reject(new Error('no Orders'));

                resolve({order : response.rows[0]});
                }
            );
        });
    }

    static getOrders() {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Orders `,
                (error, response) => {
                if (error) return reject(error);
                resolve( response.rows);
                }
            );
        });
    }

    static storeOrderInit(customerId, coordinatesInit, timestampInit, tel) {

        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Orders("customerId", "coordinatesInit" , "timestampInit", tel)
                VALUES($1, $2, $3, $4)`,

                [customerId, coordinatesInit, timestampInit, tel],
                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderAction2(coordinatesDriver, timestampStarted, driverId) {

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "coordinatesDriver" = $1, "timestampStarted" = $2 
                WHERE "driverId" = $3`,
                [coordinatesDriver, timestampStarted, driverId],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }
    
    static storeOrderStopped(coordinatesDest ,timestampStopped, driverId) {

        console.log("A4", coordinatesDest, timestampStopped, driverId);

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "coordinatesDest" = $1, "timestampStopped" = $2 
                WHERE "driverId" = $3`,
                [coordinatesDest ,timestampStopped, driverId],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }
    static updateOrderStarted(driveId, coordinatesInitDriver, timestampStarted ,id) {
        console.log(timestampStarted);
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "driverId" = $1, "coordinatesInitDriver" = $2, "timestampStarted" = $3
                WHERE "customerId" = $4`,
                [driveId, coordinatesInitDriver, timestampStarted, id],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderWaiting(timestampWaiting, driveId) {
        console.log(driveId);
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampWaiting" = $1
                WHERE "driverId" = $2`,
                [timestampWaiting, driveId],
                
                (error, response) => {
                if (error) return reject(error);
                    console.log(timestampWaiting)
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderArrive(timestampArrive, driveId) {
        console.log(driveId);
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampArrive" = $1
                WHERE "driverId" = $2`,
                [timestampArrive, driveId],
                
                (error, response) => {
                if (error) return reject(error);
                    console.log(timestampArrive)
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderDriving(timestampDrive, driveId) {
        console.log(driveId);
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampDrive" = $1
                WHERE "driverId" = $2`,
                [timestampDrive, driveId],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderBilling(timestampBilling, timestampPayed, driveId) {
        console.log(driveId);
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampBilling" = $1, "timestampPayed" = $2
                WHERE "driverId" = $3`,
                [timestampBilling, timestampPayed, driveId],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderClosed(timestampClosed, driveId) {
        console.log(driveId);
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampClosed" = $1
                WHERE "driverId" = $2`,
                [timestampClosed, driveId],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }

  
}

module.exports = OrdersTable;
