const pool = require('../../databasePool');

class VoiceTable {
    static storeVoice(customerId, driverId, voicePath, timestamp) {
        console.log("A5", voicePath);

        return new Promise((resolve, reject) => {
            pool.query(
                `INSERT INTO Voice ("customerId", "driverId", "voicePath", timestamp)
                VALUES ($1, $2, $3, $4) 
                `,
                [customerId, driverId,voicePath, timestamp],
                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }
    static getVoice() {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT *
                FROM Voice
                `,

                (error, response) => {
                if (error) return reject(error);

                if (response.rows.length === 0) return reject(new Error('no users'));

                resolve(response.rows);
                }
            );
        });
    }

    static getVoiceById(customerId) {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "voicePath", timestamp, "voiceFile"
                FROM Voice WHERE "customerId" = $1
                `,
                [customerId],
                (error, response) => {
                if (error) return reject(error);

                resolve({voice:response.rows[0]});
                }
            );
        });
    }

    static deleteVoiceById(customerId) {

        return new Promise((resolve, reject) => {
            pool.query(
                `DELETE "voicePath"
                FROM Voice WHERE "customerId" = $1
                `,
                [customerId],
                (error, response) => {
                if (error) return reject(error);

                if (response.rows.length === 0) return reject(new Error('no users'));

                resolve(response.rows);
                }
            );
        });
    }


}

module.exports = VoiceTable;