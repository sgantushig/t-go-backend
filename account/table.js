const pool = require('../../databasePool');
//const { STARTING_BALANCE } = require('../config');

class AccountTable {
  static storeAccount({ tel, pinHash, name, avatar, driver }) {
    return new Promise((resolve, reject) => {
      pool.query(
        `INSERT INTO Account(tel, "pinHash", name, avatar, driver)
         VALUES($1, $2, $3, $4, $5)`,
        [tel, pinHash, name, avatar, driver],
        (error, response) => {
          if (error) return reject(error);

          resolve({ msg: 'success!'});
        }
      );
    });
  }

  static getAccount(tel) {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT * FROM Account
         WHERE tel = $1`,
        [tel],
        (error, response) => {
          if (error) return reject(error);

          resolve({ account: response.rows[0] });
        }
      )
    });
  }

  static getAccounts(driver) {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT * FROM Account WHERE driver = $1 AND "startCoordinates" IS NOT NULL`,
        [driver],
        (error, response) => {
          if (error) return reject(error);

          resolve({ drivers: response.rows });
        }
      )
    });
  }

  static getAccountsAdmin() {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT * FROM Account WHERE "startCoordinates" IS NOT NULL`,
        [],
        (error, response) => {
          if (error) return reject(error);

          resolve( {accounts:response.rows} );
        }
      )
    });
  }

  static getAccountsTable() {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT * FROM Account`,
        
        (error, response) => {
          if (error) return reject(error);

          resolve(response.rows);
        }
      )
    });
  }

  // static updateSessionId({ sessionId, usernameHash }) {
  //   return new Promise((resolve, reject) => {
  //     pool.query(
  //       'UPDATE account SET "sessionId" = $1 WHERE "usernameHash" = $2',
  //       [sessionId, usernameHash],
  //       (error, response) => {
  //         if (error) return reject(error);

  //         resolve();
  //       }
  //     )
  //   });
  // }

  //, [account.pinHash, account.tel]

  static updateAccount({ account }) {

    return new Promise((resolve, reject) => {
      pool.query(

        `UPDATE Account SET "pinHash" = $1, name = $2, avatar = $3, "startCoordinates" = $4, "endCoordinates" = $5 WHERE tel = $6`,
        [account.pinHash, account.name, account.avatar, account.startCoordinates, account.endCoordinates, account.tel],
        (error, response) => {
          if (error) return reject(error);

          resolve();
        }
      )
    });
  }
}

module.exports = AccountTable;
