const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const GenerationEngine = require('./generation/engine');

const accountRouter = require('./api/account');
const customerRouter = require('./api/customer');
const driverRouter = require('./api/driver');
const balanceRouter = require('./api/balance');
const contactsRouter = require('./api/contacts');
const counterRouter = require('./api/counter');
const devicesRouter = require('./api/devices');
const driver_rejectsRouter = require('./api/driver_rejects');
const faqsRouter = require('./api/faqs');
const feedbackRouter = require('./api/feedback');
const filesRouter = require('./api/files');
const google_mapRouter = require('./api/google_map');
const guestRouter = require('./api/guest');
const locationsRouter = require('./api/locations');
const logRouter = require('./api/log');
const mylocationsRouter = require('./api/mylocations');
const mylocations_defaultRouter = require('./api/mylocations_default');
const newsRouter = require('./api/news');
const order_logRouter = require('./api/order_log');
const ordersRouter = require('./api/orders');
const otpRouter = require('./api/otp');
const pagesRouter = require('./api/pages');
const parametersRouter = require('./api/parameters');
const productsRouter = require('./api/products');
const products_categoryRouter = require('./api/products_catergory');
const settingsRouter = require('./api/settings');
const slidesRouter = require('./api/slides');
const socket_logRouter = require('./api/socket_log');
const stats_sell_monthRouter = require('./api/stats_sell_month');
const surpriseRouter = require('./api/surprise');
const trackingRouter = require('./api/tracking');
const usersRouter = require('./api/users');
const adminRouter = require('./api/admin');
const driveRouter = require('./api/drive');
const voiceRouter = require('./api/voice');
const ordersTemporaryRouter = require('./api/ordersTemporary');

// const newsRouter = require('./api/news');
// const ordersRouter = require('./api/orders');

const app = express();

const engine = new GenerationEngine();

engine.start();

app.locals.engine = engine;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use('/account', accountRouter);
app.use('/customer', customerRouter);
app.use('/driver', driverRouter);
app.use('/balance', balanceRouter);
app.use('/contacts', contactsRouter);
app.use('/admin', adminRouter);
app.use('/counter', counterRouter);
app.use('/devices', devicesRouter);
app.use('/driver_rejects', driver_rejectsRouter);
app.use('/faqs', faqsRouter);
app.use('/feedback', feedbackRouter);
app.use('/files', filesRouter);
app.use('/google_map', google_mapRouter);
app.use('/guest', guestRouter);
app.use('/locations', locationsRouter);
app.use('/log', logRouter);
app.use('/mylocations', mylocationsRouter);
app.use('/mylocations_default', mylocations_defaultRouter);

app.use('/news', newsRouter);
app.use('/order_log', order_logRouter);
app.use('/orders', ordersRouter);
app.use('/otp', otpRouter);
app.use('/pages', pagesRouter);

app.use('/parameters', parametersRouter);
app.use('/products', productsRouter);
app.use('/products_category', products_categoryRouter);
app.use('/settings', settingsRouter);
app.use('/slides', slidesRouter);

app.use('/socket_log', socket_logRouter);
app.use('/stats_sell_month', stats_sell_monthRouter);
app.use('/surprise', surpriseRouter);
app.use('/tracking', trackingRouter);
app.use('/users', usersRouter);

app.use('/drive', driveRouter);
app.use('/voice', voiceRouter);
app.use('/ordersTemporary', ordersTemporaryRouter);

//app.use('/news', newsRouter);

// app.use((err, req, res, next) => {
//   const statusCode = err.statusCode || 500;

//   console.error('logging error', err);

//   res.status(statusCode).json({
//     type: 'error', message: err.message
//   });
// });


module.exports = app;